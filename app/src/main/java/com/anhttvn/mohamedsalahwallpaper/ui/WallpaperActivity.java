package com.anhttvn.mohamedsalahwallpaper.ui;

import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.mohamedsalahwallpaper.R;
import com.anhttvn.mohamedsalahwallpaper.adapter.WallpaperAdapter;
import com.anhttvn.mohamedsalahwallpaper.databinding.ActivityWallpaperBinding;
import com.anhttvn.mohamedsalahwallpaper.model.Wallpaper;
import com.anhttvn.mohamedsalahwallpaper.util.BaseActivity;
import com.anhttvn.mohamedsalahwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class WallpaperActivity extends BaseActivity implements WallpaperAdapter.EventOnclick {
  private ActivityWallpaperBinding wallpaperBinding;
  private List<Wallpaper> wallpapers;
  private WallpaperAdapter adapter;

  @Override
  public void init() {
    databaseReference = FirebaseDatabase.getInstance().getReference();
    wallpaperBinding.progress.getRoot().setVisibility(View.VISIBLE);
    wallpaperBinding.header.left.setVisibility(View.GONE);
    wallpaperBinding.header.right.setVisibility(View.GONE);
    wallpaperBinding.data.getRoot().setVisibility(View.GONE);
    wallpaperBinding.header.title.setText(getString(R.string.loading));
    this.loadData();

    wallpaperBinding.header.left.setOnClickListener(v -> {
      finish();
    });

    wallpaperBinding.data.back.setOnClickListener(v -> {
      finish();
    });
  }

  @Override
  public View contentView() {
    wallpaperBinding = ActivityWallpaperBinding.inflate(getLayoutInflater());
    return wallpaperBinding.getRoot();
  }


  protected void adapter(List<Wallpaper> wallpapers) {
    wallpaperBinding.header.title.setText(getString(R.string.wallpaper));
    wallpaperBinding.progress.getRoot().setVisibility(View.GONE);
    if (wallpapers.size() > 0) {
      wallpaperBinding.header.left.setVisibility(View.VISIBLE);
      wallpaperBinding.header.right.setVisibility(View.GONE);
      wallpaperBinding.data.getRoot().setVisibility(View.GONE);
      adapter = new WallpaperAdapter(this, wallpapers,this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
      wallpaperBinding.list.setLayoutManager(layoutManager);
      wallpaperBinding.list.setItemAnimator(new DefaultItemAnimator());
      wallpaperBinding.list.setAdapter(adapter);
      adapter.notifyDataSetChanged();
    } else {
      wallpaperBinding.header.getRoot().setVisibility(View.GONE);
      wallpaperBinding.list.setVisibility(View.GONE);
      wallpaperBinding.data.getRoot().setVisibility(View.VISIBLE);
    }
  }

  protected void loadData() {
    wallpapers = new ArrayList<>();
    DatabaseReference ref2 =  databaseReference.child(Config.KEY_WALLPAPER);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Wallpaper data = dsp.getValue(Wallpaper.class);
          wallpapers.add(data);

        }
        adapter(wallpapers);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
       adapter(wallpapers);
      }
    });
  }

  @Override
  public void onClick(int position) {
    Intent intent = new Intent(this, HomeActivity.class);
    intent.putExtra(Config.KEY_BIND_WALLPAPER, wallpapers.get(position));
    startActivity(intent);
  }
}
