package com.anhttvn.mohamedsalahwallpaper.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anhttvn.mohamedsalahwallpaper.model.Photo;
import com.anhttvn.mohamedsalahwallpaper.model.Wallpaper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {
  public DatabaseHandler(Context context) {
    super(context, ConfigData.DATABASE, null, ConfigData.VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(ConfigData.CREATE_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(ConfigData.DELETE_TABLE);
    onCreate(db);
  }

  public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onUpgrade(db, oldVersion, newVersion);
  }

  public void addWallpaper(Photo photo, boolean flag) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(ConfigData.ID, photo.getId());
    values.put(ConfigData.TITLE, photo.getTitle());
    values.put(ConfigData.PATH, photo.getPath());
    values.put(ConfigData.LIKE, photo.getLike());
    values.put(ConfigData.VIEW, photo.getView());
    values.put(ConfigData.ADS, photo.isAds() ? 1 : 0);
    values.put(ConfigData.IS_LIKE, photo.isFavorite() ? 1: 0);
    if (flag) {
      db.insert(ConfigData.TABLE, null, values);
    } else {
      db.update(ConfigData.TABLE, values, ConfigData.ID + "=?", new String[]{photo.getId()});
    }

    db.close();
  }


  public boolean isWallpaper(String id) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.ID + " = ?", new String[]{String.valueOf(id)},
            null, null, null);
    cursor.moveToFirst();
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }


  public boolean isLike(String id) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.ID + " =?" + " AND " + ConfigData.IS_LIKE + " =?",
            new String[]{String.valueOf(id), String.valueOf(1)},
            null, null, null);
    cursor.moveToFirst();
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

  public int countView(String id) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.ID + " = ?", new String[]{String.valueOf(id)},
            null, null, null);
    cursor.moveToFirst();
    Photo photo = new Photo();
    while (!cursor.isAfterLast()) {
      photo.setId(cursor.getString(0));
      photo.setTitle(cursor.getString(1));
      photo.setPath(cursor.getString(2));
      photo.setLike(cursor.getInt(3));
      photo.setView(cursor.getInt(4));
      photo.setAds(cursor.getInt(5) > 0);
      photo.setFavorite(cursor.getInt(6) > 0);
      cursor.moveToNext();
    }
    cursor.close();
    return photo.getView();
  }

  public ArrayList<Photo> photos() {
    ArrayList<Photo> list = new ArrayList<>();
    String query = "SELECT * FROM " + ConfigData.TABLE;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Photo photo = new Photo();
      photo.setId(cursor.getString(0));
      photo.setTitle(cursor.getString(1));
      photo.setPath(cursor.getString(2));
      photo.setLike(cursor.getInt(3));
      photo.setView(cursor.getInt(4));
      photo.setAds(cursor.getInt(5) > 0);
      photo.setFavorite(cursor.getInt(6) > 0);
      list.add(photo);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }

  public ArrayList<Photo> likePhotos() {
    ArrayList<Photo> list = new ArrayList<>();
//    String query = "SELECT * FROM " +ConfigData.TABLE + " WHERE " + ConfigData.IS_LIKE + " = 1"  ;
    SQLiteDatabase db = this.getReadableDatabase();
//    Cursor cursor = db.rawQuery(query, null);

    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.IS_LIKE + " = ?", new String[] { String.valueOf(1)},
            null, null, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Photo photo = new Photo();
      photo.setId(cursor.getString(0));
      photo.setTitle(cursor.getString(1));
      photo.setPath(cursor.getString(2));
      photo.setLike(cursor.getInt(3));
      photo.setView(cursor.getInt(4));
      photo.setAds(cursor.getInt(5) > 0);
      photo.setFavorite(cursor.getInt(6) > 0);
      list.add(photo);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }

}
