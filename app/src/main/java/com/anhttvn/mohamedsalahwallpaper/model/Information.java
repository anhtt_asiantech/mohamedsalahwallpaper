package com.anhttvn.mohamedsalahwallpaper.model;

import lombok.Data;

@Data
public class Information {
  public String wallpaperOnline;
  public String wallpaperOffline;
  public String dateUpdate;
  public String dateUpdateApp;
  public String version;
}
