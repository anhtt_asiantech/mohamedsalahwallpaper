package com.anhttvn.mohamedsalahwallpaper.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Photo implements Serializable {
  public String id;
  public String path;
  public String title;
  public int view;
  public boolean ads;
  public int like;
  public boolean favorite;
}
