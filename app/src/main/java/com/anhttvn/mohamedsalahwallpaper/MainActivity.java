package com.anhttvn.mohamedsalahwallpaper;


import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ShareCompat;

import com.anhttvn.mohamedsalahwallpaper.databinding.ActivityMainBinding;
import com.anhttvn.mohamedsalahwallpaper.model.Notification;
import com.anhttvn.mohamedsalahwallpaper.model.Photo;
import com.anhttvn.mohamedsalahwallpaper.ui.HomeActivity;
import com.anhttvn.mohamedsalahwallpaper.ui.NotificationActivity;
import com.anhttvn.mohamedsalahwallpaper.ui.SettingActivity;
import com.anhttvn.mohamedsalahwallpaper.util.BaseActivity;
import com.anhttvn.mohamedsalahwallpaper.util.Config;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {
  private static final int MY_REQUEST_CODE = 500;
  private ActivityMainBinding mainBinding;
  private ArrayList<Notification> notifications;

  private AppUpdateManager appUpdateManager;

  @Override
  public void init() {
    databaseReference = FirebaseDatabase.getInstance().getReference();
    if (isConnected()) {
      this.loadDataNotification();
    }

    this.event();
    this.isBannerADS(mainBinding.ads);
    checkForUpdate();
  }

  @Override
  public View contentView() {
    mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
    return mainBinding.getRoot();
  }

  protected void event() {
    mainBinding.menu.viewNo.setVisibility(View.GONE);

    mainBinding.menu.wallpaper.setOnClickListener(v -> {
      Intent intent = new Intent(this, HomeActivity.class);
      intent.putExtra(Config.KEY_MAIN, Config.KEY.WALLPAPER);
      startActivity(intent);
    });

    mainBinding.menu.favorite.setOnClickListener(v -> {
      Intent intent = new Intent(this, HomeActivity.class);
      intent.putExtra(Config.KEY_MAIN, Config.KEY.FAVORITE);
      startActivity(intent);
    });

    mainBinding.menu.download.setOnClickListener(v -> {
      Intent intent = new Intent(this, HomeActivity.class);
      intent.putExtra(Config.KEY_MAIN, Config.KEY.DOWNLOAD);
      startActivity(intent);
    });

    mainBinding.menu.notification.setOnClickListener(v -> {
      if (notifications != null && notifications.size() > 0) {
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra(Config.KEY_SEND_NOTIFICATION, notifications);
        startActivity(intent);
      } else {
       showToast(getString(R.string.no_notification));
      }

    });

    mainBinding.setting.setOnClickListener(v -> {
      PopupMenu popup = new PopupMenu(this, v);
      popup.setOnMenuItemClickListener(MainActivity.this);
      popup.inflate(R.menu.setting_menu);
      popup.show();
    });

  }

  protected void loadDataNotification() {
    notifications = new ArrayList<>();
    DatabaseReference ref2 =  databaseReference.child(Config.KEY_NOTIFICATION);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Notification data = dsp.getValue(Notification.class);
          notifications.add(data);
        }
        Collections.reverse(notifications);
        if (notifications.size() > 0) {
          mainBinding.menu.viewNo.setVisibility(View.VISIBLE);
        }
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
      }
    });
  }


  @Override
  public boolean onMenuItemClick(MenuItem item) {
    int id = item.getItemId();
    Intent intent = null;
    switch(id) {
      case R.id.privacy:
        intent = new Intent(this, SettingActivity.class);
        intent.putExtra(Config.KEY_SETTING, Config.SETTING_KEY.PRIVACY);
        startActivity(intent);
        return true;
      case R.id.information:
        intent = new Intent(this, SettingActivity.class);
        intent.putExtra(Config.KEY_SETTING, Config.SETTING_KEY.INFORMATION);
        startActivity(intent);
        return true;
      case R.id.shareApp:
        ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setChooserTitle(this.getString(R.string.app_name))
                .setText("http://play.google.com/store/apps/details?id=" + this.getPackageName())
                .startChooser();
        return true;
      default:
        return false;
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    // check number wallpaper like
    List<Photo> wallpaperLike = db.likePhotos();
    mainBinding.menu.numberLearned.setText(wallpaperLike.size() > 9 ? "9+" : String.valueOf(wallpaperLike.size()));

    appUpdateManager
            .getAppUpdateInfo()
            .addOnSuccessListener(appUpdateInfo -> {
              if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackbarForCompleteUpdate();
              }
            });
  }

  @Override
  public void onBackPressed() {
    showExit();
  }

  private void showExit() {
    AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
    LayoutInflater inflater	= this.getLayoutInflater();
    View dialogView	= inflater.inflate(R.layout.question, null);
    isBannerADS(dialogView.findViewById(R.id.ads));
    dialogBuilder.setView(dialogView);
    AlertDialog b = dialogBuilder.create();

    dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> b.dismiss());
    dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
      b.dismiss();
      finish();
    });
    b.show();

  }

  private void checkForUpdate() {
    appUpdateManager = AppUpdateManagerFactory.create(this);

// Returns an intent object that you use to check for an update.
    Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

// Checks that the platform will allow the specified type of update.
    appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
      if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
              // This example applies an immediate update. To apply a flexible update
              // instead, pass in AppUpdateType.FLEXIBLE
              && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
        // Request the update.

        try {
          appUpdateManager.startUpdateFlowForResult(
                  // Pass the intent that is returned by 'getAppUpdateInfo()'.
                  appUpdateInfo,
                  // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                  AppUpdateType.IMMEDIATE,
                  // The current activity making the update request.
                  this,
                  // Include a request code to later monitor this update request.
                  MY_REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
          e.printStackTrace();
        }
      }
    });

    appUpdateManager.registerListener(listener);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == MY_REQUEST_CODE) {
      if (resultCode != RESULT_OK) {
        Log.d("Update flow failed: ", "bb" + requestCode);
        // If the update is cancelled or fails,
        // you can request to start the update again.
      }
    }
  }

  InstallStateUpdatedListener listener = state -> {
    if (state.installStatus() == InstallStatus.DOWNLOADED) {
      // After the update is downloaded, show a notification
      // and request user confirmation to restart the app.
      popupSnackbarForCompleteUpdate();
    }
  };

  private void popupSnackbarForCompleteUpdate() {
    Snackbar snackbar =
            Snackbar.make(
                    findViewById(android.R.id.content),
                    "An update has just been downloaded.",
                    Snackbar.LENGTH_INDEFINITE);
    snackbar.setAction("INSTALL", view -> appUpdateManager.completeUpdate());
    snackbar.setActionTextColor(
            getResources().getColor(android.R.color.white));
    snackbar.show();
  }

  @Override
  protected void onStop() {
    super.onStop();
    appUpdateManager.unregisterListener(listener);
  }
}