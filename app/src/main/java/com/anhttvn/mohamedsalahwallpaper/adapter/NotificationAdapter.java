package com.anhttvn.mohamedsalahwallpaper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.mohamedsalahwallpaper.R;
import com.anhttvn.mohamedsalahwallpaper.model.Notification;

import java.util.List;
import java.util.Locale;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
  private List<Notification> notifications;
  private Context mContext;
  public NotificationAdapter(List<Notification> list, Context context) {
    notifications = list;
    mContext = context;
  }

  @NonNull
  @Override
  public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext)
            .inflate(R.layout.adapter_notification,parent,false);
    return new NotificationViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
    Notification notification = notifications.get(position);
    if (notification != null) {
      holder.date.setText(notification.getDate());
      if (Locale.getDefault().getLanguage().equalsIgnoreCase("vi")) {
        holder.content.setText(notification.getContentVi());
      } else {
        holder.content.setText(notification.getContentEn());
      }

    }
  }

  @Override
  public int getItemCount() {
    return notifications.size();
  }

  public class NotificationViewHolder extends RecyclerView.ViewHolder {

    protected TextView date;
    protected TextView content;
    public NotificationViewHolder(@NonNull View itemView) {
      super(itemView);

      date = itemView.findViewById(R.id.date);
      content = itemView.findViewById(R.id.content);
    }
  }
}
